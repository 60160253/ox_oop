
public class Board {
	
	private Player X;
	private Player O;
	private Player draw;
	private Player currentPlayer;
	private boolean chk;
	private int turnCount;
	private String[][] table;
	
	public Board(Player X, Player O) {
		this.X = X;
		this.O = O;
		currentPlayer = X;
	}
	
	public String[][] getTable() {
		table = new String[3][3];
		for(int i=0; i<table.length; i++) {
			for(int j=0; j<table.length; j++ ) {
				table[i][j] = " "; 
			}
		}
		return table;
	}
	
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	
	public void switchPlayer() {
		if (currentPlayer == X) {
			currentPlayer = O;
		}else {
			currentPlayer = X;
		}
		turnCount++;
	}
	
	public boolean isFinish() {
		return false;
	}
	
	public Player checkWin() {
		for(int i=0; i<8; i++) {
			String line = null;
			switch (i) {
			case 0:
				line = table[0][0] + table[0][1] + table[0][2];
				break;
			case 1:
				line = table[1][0] + table[1][1] + table[1][2];
				break;
			case 2:
				line = table[2][0] + table[2][1] + table[2][2];
				break;
			case 3:
				line = table[0][0] + table[1][0] + table[2][0];
				break;
			case 4:
				line = table[0][1] + table[1][1] + table[2][1];
				break;
			case 5:
				line = table[0][2] + table[1][2] + table[2][2];
				break;	
			case 6:
				line = table[0][0] + table[1][1] + table[2][2];
				break;
			case 7:
				line = table[2][0] + table[1][1] + table[0][2];
				break;
			}
			if (line.equals("XXX")) {
				return X;
			}else if (line.equals("OOO")) {
				return O;
			}
		}
		chk = false;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (table[i][j] == " ") {
					chk = true;
				}
			}
		}
		if(chk == false) {
			return currentPlayer = draw;
		}else {
			System.out.println("Plz choose position (R,C): ");
			return null;
		}
	}
	
}
